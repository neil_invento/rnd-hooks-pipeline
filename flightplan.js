const plan = require("flightplan");
const path = require("path");
const fs = require("fs");
const config = require("./flightplan.config");
const buildDir = config.BUILD_DIR;
const tmpDir = config.UPLOAD_DIR_NAME;
const deployPath = config.DEPLOY_PATH;

const getAllFiles = dir => {
    return fs.readdirSync(dir).reduce((files, file) => {
        const name = path.join(dir, file);
        const isDirectory = fs.statSync(name).isDirectory();

        return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
    }, []);
};
// configuration
plan.target("staging", {
    host: "localhost",
    username: "neil",
    privateKey: "/home/neil/.ssh/my_key.pem",
    agent: process.env.SSH_AUTH_SOCK,
});

plan.local(["default", "build"], local => {
    local.log("Building app...");
    local.exec("npm run build");
});

plan.local(["default", "sync"], local => {
    local.log("Syncing to staging server...");
    local.transfer(getAllFiles(buildDir), tmpDir);
});

plan.remote(["default", "deploy"], remote => {
    remote.log("Executing Remote Commands...");
    remote.log(`Copying from ${tmpDir} to ${buildDir}`);
    remote.sudo(`rm -rf ${deployPath}`, { user: "neil" });
    remote.sudo(`mkdir -p ${deployPath}`, { user: "neil" });
    remote.sudo(`cp -Rf ${tmpDir}/build/* ${deployPath}`, { user: "neil" });
    remote.log("Cleanup temp folder", { user: "neil" });
    remote.sudo(`rm -rf ${tmpDir}/*`, { user: "neil" });
});
